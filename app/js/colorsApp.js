var app = angular.module("colors", ["ngRoute"]);

intdiv = function(numerator, denominator) {
    var result = numerator / denominator;
    if (result >= 0) {
        return Math.floor(result);
    } else {
        return Math.ceil(result);
    }
}

app.config(function($routeProvider) {
  $routeProvider
  .when("/calculator", {
    templateUrl : "calculator.html",
    controller : "colorController",
    controllerAs : "ctrl"
  })
  .otherwise({
      templateUrl : "hello.html"
  });
});

app.controller("colorController", function(colorPopulator) {
    this.colors = [
      {name: "Black", value: 0, multiplier:'', tolerance: '1%'},
      {name: "Brown", value: 1, multiplier: '0', tolerance: '2%'} ,
      {name: "Red", value: 2, multiplier: '00'},
      {name: "Orange", value: 3, multiplier: 'K'},
      {name: "Yellow", value: 4, multiplier: '0K'},
      {name: "Green", value: 5, multiplier: '00K', tolerance: '0.5%'},
      {name: "Blue", value: 6, multiplier: 'M', tolerance:'0.25%'},
      {name: "Violet", value: 7, multiplier: '0M', tolerance:'0.1%'},
      {name: "Grey", value: 8, multiplier: '00M', tolerance: '0.05%'},
      {name: "White", value: 9, multiplier: 'G'},
      {name: "Gold", multiplier: '*10^-1', tolerance: '5%'},
      {name: "Silver", multiplier: '*10^-2', tolerance: '10%'},
      {name: "None"}
    ];
    this.colorBands = [];
    this.showResistance = false;
    this.createBands = function() {
      colorPopulator.clearBand();
      this.showResistance = true;
      this.colorBands.forEach(function(color) {
          colorPopulator.addColor(color);
        });

      console.log(colorPopulator.getColors()[0]);
      console.log(colorPopulator.getColors()[1]);
      console.log(colorPopulator.getColors()[2]);
      console.log(colorPopulator.getColors()[3]);
      console.log(colorPopulator.getColors()[4]);
    };
});

app.factory('colorPopulator', function() {
    var colorBands = [];
    return {
      getColors: function() { return colorBands; },

      clearBand: function() { colorBands = []; },

      addColor: function(color) {
          if (!color) {
            return;
          }
          colorBands.push(color);
      }
    }
});
