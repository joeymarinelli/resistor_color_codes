describe('Service: colorPopulator#addColor', function () {
  beforeEach(module('colors'));
  var service;

  beforeEach(inject(function (_colorPopulator_) {
    service = _colorPopulator_;
  }));

  it('should have color array consist of red', function() {
    service.addColor({name: 'Red', value: 2});
    expect(service.getColors()).toEqual([{name: 'Red', value: 2}]);
  });

  it('should have color array consist of red, green and blue', function() {
    service.addColor({name: 'Red', value: 2});
    service.addColor({name: 'Green', value: 3});
    service.addColor({name: 'Blue', value: 4});
    expect(service.getColors()).toEqual([{name: 'Red', value: 2}, {name: 'Green', value: 3}, {name: 'Blue', value: 4}]);
  });

  it('should have color value of 3', function() {
    service.addColor({name: 'Red', value: 3});
    expect(service.getColors()[0].value).toEqual(3);
  });

  it('should have color value of 1', function() {
    service.addColor({name: 'Brown', value: 1});
    expect(service.getColors()[0].value).toEqual(1);
  });

  it('should have color multiplier of M', function() {
    service.addColor({name: 'Brown', value: 1, multiplier: 'M'});
    expect(service.getColors()[0].multiplier).toEqual('M');
  });

  it('should have color tolerance of 10%', function() {
    service.addColor({name: 'Brown', value: 1, multiplier: 'M', tolerance: '10%'});
    expect(service.getColors()[0].tolerance).toEqual('10%');
  });

});
