function dumpHtml() {
   browser.getPageSource().then(function(source) {
      console.log(source);
   });
}

describe('Calculator Test', function() {
  it('should have a total of 5 color bands', function() {
    browser.get('/main.html#!/calculator');
    var colorBands = element.all(
      by.repeater('color in [1, 2, 3, 4, 5]'));

      expect(colorBands.count()).toEqual(5);
  });

  it('should select blue as the second color', function() {
    browser.get('/main.html#!/calculator');
    var secondColor = element(
      by.id('Color_2'));
    secondColor.sendKeys('Blue');
    expect(secondColor.getAttribute('value')).toEqual('Blue');
  });

  it('should select black as the third color', function() {
    browser.get('/main.html#!/calculator');
    var thirdColor = element(
      by.id('Color_3'));
    thirdColor.sendKeys('Black');
    expect(thirdColor.getAttribute('value')).toEqual('Black');
  });

  it('should select grey as the fourth color', function() {
    browser.get('/main.html#!/calculator');
    var fourthColor = element(
      by.id('Color_4'));
    fourthColor.sendKeys('Grey');
    expect(fourthColor.getAttribute('value')).toEqual('Grey');
  });

  it('should select gold as the fifth color', function() {
    browser.get('/main.html#!/calculator');
    var fifthColor = element(
      by.id('Color_5'));
    fifthColor.sendKeys('Gold');
    expect(fifthColor.getAttribute('value')).toEqual('Gold');
  });

  it('should select none as the fifth color', function() {
    browser.get('/main.html#!/calculator');
    var fifthColor = element(
      by.id('Color_5'));
    fifthColor.sendKeys('None');
    expect(fifthColor.getAttribute('value')).toEqual('None');
  });

  it('should calculate to 264K ohms with +/- 5%', function () {
    browser.get('/main.html#!/calculator');
    var firstColor = element(
      by.id('Color_1'));
    var secondColor = element(
      by.id('Color_2'));
    var thirdColor = element(
      by.id('Color_3'));
    var fourthColor = element(
      by.id('Color_4'));
    var fifthColor = element(
      by.id('Color_5'));
    var calculateButton = element(
      by.id('calculatorButton'));
    var resistance = element(
      by.id('resistance'));
    firstColor.sendKeys('Red');
    secondColor.sendKeys('Blue');
    thirdColor.sendKeys('Yellow');
    fourthColor.sendKeys('Orange');
    fifthColor.sendKeys('Gold');
    calculateButton.click();
    expect(resistance.getText()).toEqual('264K Ω +/- 5%');
  });

});
