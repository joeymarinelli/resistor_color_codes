function dumpHtml() {
   browser.getPageSource().then(function(source) {
      console.log(source);
   });
}

describe('Routing Test', function() {

  it('should show the welcome page', function() {
    browser.get('/main.html');
    var welcome = element(
      by.id('welcome'));
    expect(welcome.getText()).toEqual('Welcome');
  });

  it('should navigate the user to the calculator page', function() {
    browser.get('/main.html');
    element(by.id('calculatorLink')).click();

    expect(browser.getCurrentUrl()).toEqual('http://localhost:8000/main.html#!/calculator');
  });

});
